public class Kata1 {
    public String fizzBuzz(int x) {
        if (x % 15 == 0)
            return "FizzBuzz";
        if (x % 3 == 0) {
            return "Fizz";
        } else if (x % 5 == 0)
            return "Buzz";
        else
            return Integer.toString(x);
    }
}
