import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class Kata1Test {
    private Kata1 mytest;

    @BeforeEach
    public void setUp() {
        mytest = new Kata1();
    }

    @Test
    public void GivenInteger_WhenInputValue_ThenOutputValue_ShouldNotSame() {
        assertNotEquals(4, mytest.fizzBuzz(4));
    }

    @Test
    public void GivenInt_ShouldReturn_Fizz() {
        assertEquals("Fizz", mytest.fizzBuzz(3));
    }

    @Test
    public void GivenInt_AllShouldReturn_Fizz() {
        assertEquals("Fizz", mytest.fizzBuzz(3));
        assertEquals("Fizz", mytest.fizzBuzz(6));
        assertEquals("Fizz", mytest.fizzBuzz(12));
        assertEquals("Fizz", mytest.fizzBuzz(33));
    }

    @Test
    public void GivenInt_ShouldNotReturn_Fizz() {
        assertNotEquals("Fizz", mytest.fizzBuzz(2));
    }


    @Test
    public void GivenInt_AllShouldReturn_Buzz() {
        assertEquals("Buzz", mytest.fizzBuzz(5));
        assertEquals("Buzz", mytest.fizzBuzz(10));
        assertEquals("Buzz", mytest.fizzBuzz(200));
    }

    @Test
    public void GivenInt_ShouldReturn_FizzBuzz() {
        assertEquals("FizzBuzz", mytest.fizzBuzz(15));
        assertEquals("FizzBuzz", mytest.fizzBuzz(30));
        assertEquals("FizzBuzz", mytest.fizzBuzz(45));
    }

}
